function onsubmitsignup(event) {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;

    if (!validateEmail(email)) {
        event.preventDefault();
        alert("Email is in invalid format")
    }
    if (password != confirmPassword) {
        event.preventDefault();
        alert("Passwords don't match");
    }
}

function onsubmitlogin(event){
    var email = document.getElementById("email").value;
    if (!validateEmail(email)) {
        event.preventDefault();
        alert("Email is in invalid format")
    }
}

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}