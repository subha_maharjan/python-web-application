import os
import psycopg2
from psycopg2 import sql
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from jinja2 import Environment, select_autoescape, FileSystemLoader
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect, secure_filename
from werkzeug.security import generate_password_hash, check_password_hash
from database.db import DBName, DBhost, DBUser, DBpassword, DBport

con = psycopg2.connect(database=DBName, host=DBhost, 
                       user=DBUser, password=DBpassword, port=DBport)
cursor = con.cursor()

# SECRET_KEY = b'\xd694\xa0\x93\x95\xeb\xf7u\xf8m\x03/T\xfa\n'

class Webapp():
    def __init__(self):
        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.env = Environment(
            loader=FileSystemLoader(template_path),
            autoescape=select_autoescape(['html', 'xml'])
        )
        self.url_map = Map(
            [
                Rule("/", endpoint="home"),
                Rule("/login", endpoint="login"),
                Rule("/signup", endpoint="signup"),
                Rule("/profile", endpoint="profile"),
                Rule("/profile/add", endpoint="add"),
                Rule("/profile/edit/<int:id>", endpoint="edit"),
                Rule("/profile/delete/<int:id>/", endpoint="delete"),
                Rule("/logout", endpoint="logout")
            ]
        )

    def on_home(self, request):
        image_names = os.listdir('static/images')
        return self.render_template("home.html", image_names=image_names)

    def on_login(self, request):
        if request.method == 'POST':
            email = request.form['email']
            password = request.form['password']
            cursor.execute(f"select email from users where email='{email}';")
            data = cursor.fetchone()
            if data is None:
                return redirect("/login")
            cursor.execute(f"select password from users where email='{email}';")
            passwordrecord = cursor.fetchone()[0]
            checkpassword = check_password_hash(passwordrecord, password)
            if checkpassword is True:
                return redirect('/profile')
            else:
                return redirect("/login")

        if request.method == 'GET':
            return self.render_template('login.html')

    def on_signup(self, request):
        if request.method == 'POST':               
            fname = request.form['fname']
            mname = request.form['mname']
            lname = request.form['lname']
            email = request.form['email']
            password = request.form['password']
            pswd = generate_password_hash(password, "sha256")
            cursor.execute(f"insert into users (firstName, middleName, lastName, email, password) \
                        values ('{fname}', '{mname}', '{lname}', '{email}', '{pswd}');")
            # cursor.execute(sql.SQL(("""
            # INSERT INTO users (firstName, middleName, lastName, email, password) VALUES \
            # ({fname}, {mname}, {lname}, {email}, {pswd})""").format(fname = sql.Literal(request.get('fname')), mname = sql.Literal(request.get('mname')), \
            #     lname = sql.Literal(request.get('lname')), \
            #     email = sql.Literal(request.get('email')), \
            #     pswd = sql.Literal(request.get('pswd')))))
            con.commit()
            return redirect('/login')
        
        if request.method == 'GET':
            return self.render_template('signup.html')

    def on_profile(self, request):
        cursor.execute("Select * from ProductList")
        data = cursor.fetchall()
        return self.render_template("profile.html", data=data)

    def on_add(self, request):
        if request.method == 'POST':
            name = request.form['name']
            brand = request.form['brand']
            size = request.form['size']
            color = request.form['color']
            price = request.form['price']
            file = request.files['image']
            if file.filename == '':
                return redirect(request.url)
            img = secure_filename(file.filename)
            path = os.path.join(os.getcwd(), 'static/images')
            img_path = os.path.join(path, img)
            file.save(img_path)
            description = request.form['description']
            cursor.execute(f"insert into ProductList (name, brand, size, color, price, image, description) \
                           values ('{name}', '{brand}', '{size}', '{color}', '{price}', '{img}', '{description}');")
            con.commit()
            return redirect('/profile')

        if request.method == 'GET':
            return self.render_template('addproduct.html')

    def on_edit(self, request, id):
        if request.method == 'POST':
            name = request.form['name']
            brand = request.form['brand']
            size = request.form['size']
            color = request.form['color']
            price = request.form['price']
            file = request.files['image']
            img = secure_filename(file.filename)
            basepath = 'static/images'
            imagelist = []
            for entry in os.listdir(basepath):
                if os.path.isfile(os.path.join(basepath, entry)):
                    imagelist.append(entry)
                    print(imagelist)
            if img not in imagelist:
                path = os.path.join(os.getcwd(), 'static/images')
                img_path = os.path.join(path, img)
                file.save(img_path)
            description = request.form['description']
            cursor.execute(f"update ProductList set name='{name}', brand='{brand}', size='{size}', color='{color}', \
                           price='{price}', image='{img}', description='{description}' where id={id};")
            con.commit()
            return redirect('/profile')

        if request.method == 'GET':
            cursor.execute(f"select name, brand, size, color,price, image, description, id from ProductList where id={id};")
            record = cursor.fetchone()
            return self.render_template("editproduct.html", record=record)

    def on_delete(self, request, id):
        cursor.execute(f"delete from ProductList where id={id}")
        con.commit()
        return redirect("/profile")

    def on_logout(self, request):
        return redirect('/')

    def error_404(self):
        response = self.render_template("404.html")
        response.status_code = 404
        return response

    def render_template(self, template_name, **context):
        t = self.env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, f"on_{endpoint}")(request, **values)
        except NotFound:
            return self.error_404()
        except HTTPException as e:
            return e

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)


def create_app(with_static=True):
    application = Webapp()
    if with_static:
        application.wsgi_app = SharedDataMiddleware(application.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), 'static')
        })
    return application

if __name__ == "__main__":
    from werkzeug.serving import run_simple
    app = create_app()
    run_simple("127.0.0.1", 8000, app, use_debugger=True, use_reloader=True)
