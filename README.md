### A Python WSGI Application with Werkzeug

Werkzeug is a Web Server Gateway Interface (WSGI) application library.

This is a simple web application where the users will do certain operations as:
* Login and Signup
* Adding Product Details including Images to DataBase
* Update/Edit the Product Details
* Delete the Product

### To install all the dependencies of this project.
`pip install -r requirements.txt`

### Major Libraries used:
* Jinja : Templates (Html files)
* Psycopg2 :  Python Adapter for postgresql
    * psycopg2-binary: Linux Users


### To run the application
```
$ python app.py
* Running on http://127.0.0.1:8000/
```

### Routing
```
self.url_map = Map([
    Rule('/', endpoint='new_url'),
    Rule('/<url_id>', endpoint='path_to_url_id')
])
```
The Map and Rule tries to match the url to the endpoint. Here we have created a URL map with two rules. `/` is for root which is dispatched to a function that implements the logic to create new url. And `/<url_id>` follows the path to that url id with new url.

The html files are placed in the **Templates** folder and Media files, Javascript and CSS files are placed in the **Static** Folder.




