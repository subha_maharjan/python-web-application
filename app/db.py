import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# connect to postgresql dbms
con = psycopg2.connect(database="mypythonapp", host="localhost", 
                       user="postgres", password="newpassword", port=5432)
con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

#obtain a db cursor
cursor = con.cursor()

# create table statement
SQL = '''create database mypythonapp;'''

#create a database in postgresql database
cursor.execute(SQL)

# create table
cursor.execute("create table Users(id SERIAL PRIMARY KEY, firstname VARCHAR(20),\
                middlename VARCHAR(20), lastname VARCHAR(20), email VARCHAR(100), password VARCHAR(50))")

cursor.execute("create table ProductList(id SERIAL PRIMARY KEY, name VARCHAR(25), \
                brand VARCHAR(20), size VARCHAR(20), color VARCHAR(50), price FLOAT(25), image VARCHAR(255), description VARCHAR(100))")
                
#commit the query in database
# con.commit()
cursor.close()

# close the connection
con.close()
